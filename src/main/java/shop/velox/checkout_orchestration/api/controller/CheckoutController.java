package shop.velox.checkout_orchestration.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.commons.rest.response.RestResponsePage;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;


@Tag(name = "Checkout", description = "the Checkout API")
@RequestMapping("/orders")
public interface CheckoutController {

  String ORDER_STATUS_FILTER = "statusFilter";

  @Operation(summary = "Creates an Order in preview status", description = "creates an Order in preview status. The cart is not deleted at this point.")
  @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Order created in preview status. It is ready to be placed.", content = @Content(schema = @Schema(implementation = OrderDto.class))),
      @ApiResponse(responseCode = "206", description = "Order created in preview status. It is not yet ready to be placed, some properties have not been set.", content = @Content(schema = @Schema(implementation = OrderDto.class))),
      @ApiResponse(responseCode = "404", description = "Cart not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "403", description = "You tried to set read-only parameters (e.g. a price)", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Cart content makes it not ready for checkout. (e.g. stock or price missing)", content = @Content(schema = @Schema()))
  })
  ResponseEntity<OrderDto> createOrder(
      @Parameter(description = "Parameters for Order.") @RequestBody CartDto cartDto);


  @Operation(summary = "Retrieves an Order", description = "Retrieves an Order")
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "Order is found",
          content = @Content(schema = @Schema(implementation = OrderDto.class)),
          headers = {@Header(name = HttpHeaders.ETAG, schema = @Schema(type = "string"), description = "It identifiers a specific version of a resource")}),
      @ApiResponse(responseCode = "206",
          description = "Order is in preview status, and it is not yet ready to be placed, some properties have not been set.",
          content = @Content(schema = @Schema(implementation = OrderDto.class)),
          headers = {@Header(name = HttpHeaders.ETAG, schema = @Schema(type = "string"), description = "It identifiers a specific version of a resource")}),
      @ApiResponse(responseCode = "304",
          description = "Order did not change since last request",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "404",
          description = "Order not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422",
          description = "Order is in preview status. Related cart content makes it not ready for checkout. (e.g. stock or price missing)",
          content = @Content(schema = @Schema()))
  })
  ResponseEntity<OrderDto> getOrder(
      @Parameter(hidden = true) @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToUserIdMapper.apply(#this)") String userId,
      @Parameter(description = "Id of the Order. Cannot be empty.", required = true) @PathVariable("id") String orderId,
      @Parameter(description = "value of the ETag header received in the previous request") @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) String cachedEtag);


  @Operation(summary = "gets all Orders", description = "Paginated. Not available to guests, only to order owner or Order Admin")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = OrderDto.class)))
  })
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  RestResponsePage<OrderDto> getOrders(
      @Parameter(description = "Id of the user that requires orders") @RequestParam(name = "userId", required = false, defaultValue = "") String userId,
      @PageableDefault(size = 3) @SortDefault.SortDefaults({@SortDefault(sort = "createTime", direction = Sort.Direction.DESC)}) Pageable pageable,
      @Parameter(description = "Return orders with different status (DRAFT, ORDERED). If statusFilter is empty, return all orders. Can be empty.")
      @RequestParam(name = ORDER_STATUS_FILTER, required = false) final OrderStatus filter);


  @Operation(summary = "Updates an Order", description = "Updates an Order")
  @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Order Updated", content = @Content(schema = @Schema(implementation = OrderDto.class))),
      @ApiResponse(responseCode = "404", description = "Order not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "403", description = "You tried to set read-only parameters (e.g. a price)", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "412", description = "Order was changed after your last request. Your version was stale", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "417", description = "Change could not be performed. (e.g. you tried to change orderStatus to Placed when shippingAddress was not set.)", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "428", description = "If-Match header is empty. If-Match header must contain your cached Order ETag value")
  })
  ResponseEntity<OrderDto> update(
      @Parameter(hidden = true) @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToUserIdMapper.apply(#this)") String userId,
      @Parameter(description = "Id of Order. Cannot be empty.", required = true) @PathVariable("id") String orderId,
      @Parameter(description = "Parameters for Order.") @RequestBody OrderDto order,
      @Parameter(description = "value of the ETag header received in the previous request", required = true) @RequestHeader(value = HttpHeaders.IF_MATCH, required = false) String cachedEtag);
}