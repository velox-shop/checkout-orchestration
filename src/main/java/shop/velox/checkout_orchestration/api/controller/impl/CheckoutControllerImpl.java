package shop.velox.checkout_orchestration.api.controller.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.checkout_orchestration.api.controller.CheckoutController;
import shop.velox.checkout_orchestration.service.CheckoutOrchestrationService;
import shop.velox.commons.rest.response.RestResponsePage;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;

@RestController
public class CheckoutControllerImpl implements CheckoutController {

  private static final Logger LOG = LoggerFactory.getLogger(CheckoutControllerImpl.class);

  private CheckoutOrchestrationService checkoutOrchestrationService;

  public CheckoutControllerImpl(@Autowired CheckoutOrchestrationService checkoutOrchestrationService) {
    this.checkoutOrchestrationService = checkoutOrchestrationService;
  }

  @Override
  public ResponseEntity<OrderDto> createOrder(final CartDto cartDto) {
    return checkoutOrchestrationService.createOrder(cartDto);
  }

  @Override
  public ResponseEntity<OrderDto> getOrder(final String userId, final String orderId, final String cachedEtag) {
    return checkoutOrchestrationService.getOrder(userId, orderId, cachedEtag);
  }

  @Override
  public RestResponsePage<OrderDto> getOrders(final String userId, Pageable pageable, final OrderStatus filter) {
    if(StringUtils.isBlank(userId)){
        LOG.debug("get all Orders as admin {}, filter {}", pageable, filter);
        return checkoutOrchestrationService.getAllOrders(pageable, filter);
    }
    LOG.debug("get user Orders as user {}, {}, filter {}", userId, pageable, filter);
    return checkoutOrchestrationService.getAllOrdersByUserId(userId, pageable, filter);
  }

  @Override
  public ResponseEntity<OrderDto> update(final String userId, final String orderId, final OrderDto order, final String cachedEtag) {
    return checkoutOrchestrationService.updateOrder(userId, orderId, order, cachedEtag);
  }
}