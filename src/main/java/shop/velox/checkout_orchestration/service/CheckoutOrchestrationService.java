package shop.velox.checkout_orchestration.service;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.commons.rest.response.RestResponsePage;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;

public interface CheckoutOrchestrationService {

  ResponseEntity<OrderDto> createOrder(CartDto cartDto);

  ResponseEntity<OrderDto> getOrder(String userId, String orderId, String cachedEtag);

  RestResponsePage<OrderDto> getAllOrders(Pageable pageable, OrderStatus filter);

  RestResponsePage<OrderDto> getAllOrdersByUserId(String userId, Pageable pageable, OrderStatus filter);

  ResponseEntity<OrderDto> updateOrder(String userId, String orderId, OrderDto order, String cachedEtag);

}
