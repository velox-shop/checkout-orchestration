package shop.velox.checkout_orchestration.converter;

import org.mapstruct.Mapper;
import shop.velox.order.api.dto.AddressDto;

@Mapper
public interface AddressConverter {

  AddressDto convertUserAddressToOrderAddress(shop.velox.user.api.dto.AddressDto addressDto);

}
