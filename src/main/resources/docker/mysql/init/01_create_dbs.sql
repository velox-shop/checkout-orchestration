# noinspection SqlNoDataSourceInspectionForFile
USE mysql;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;

# create databases
CREATE DATABASE IF NOT EXISTS `slycartdb`;
CREATE DATABASE IF NOT EXISTS `slypricedb`;
CREATE DATABASE IF NOT EXISTS `slyavailabilitydb`;
CREATE DATABASE IF NOT EXISTS `slyuserdb`;

# grant rights to slyusr
GRANT ALL PRIVILEGES ON *.* TO 'slyusr'@'%';
FLUSH PRIVILEGES;