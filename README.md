# Work locally
Build Service
```
./gradlew clean build --refresh-dependencies
```

Execute dependencies with Docker Compose and this service with Gradle:
```
docker-compose --file docker-compose.dependencies.yml up
./gradlew bootRun
```

Execute everything with Docker Compose
```
docker-compose --file docker-compose.dependencies.yml --file docker-compose.this.yml up --build --force-recreate --remove-orphans --renew-anon-volumes
```

Test
```
newman run src/test/*postman_collection.json --environment src/test/veloxCheckoutOrchestration-local.postman_environment.json --reporters cli,html --reporter-html-export newman-results.html
```

## API documentation
Online API Documentation:
- HTML: http://localhost:8442/checkout-orchestration/v1/swagger-ui.html
- JSON: http://localhost:8442/checkout-orchestration/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/checkout-orchestration/

### Checkout-Orchestration Service images

|Version      | Description                                                                                                                                                                                 |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0       | BREAKING CHANGE: Service uses new Cart Service DTO (new Cart DTO has to be passed when Order is created)                                                                              |
|     ...     | ...                                                                                                                                                                                         |
| 1.0.0 (1.0) | Base image                                                                                                                                                                                  |
